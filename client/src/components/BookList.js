import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import { getBooksQuery } from '../queries/queries';
import BookDetails from './BookDetails';

class BookList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null
    }
  }
  
  render() {
    const { data } = this.props;
    return (
      <div>
        <ul className="BookList">
          {data.loading ?
            <div>Loading Books...</div>

          : data.books.map(book =>
              <li key={book.id} onClick={e =>
                this.setState({ selected: book.id })
              }>
                {book.name}
              </li>
            )
          }
        </ul>
        <BookDetails bookId={this.state.selected}/>
      </div>
    );
  }
}

export default graphql(getBooksQuery)(BookList);
