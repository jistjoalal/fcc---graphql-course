import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import { getBookQuery } from '../queries/queries';

class BookDetails extends Component {

  displayBookDetails = () => {
    const { book } = this.props.data;
    if (book) {
      return (
        <div>
          <h2>{book.name}</h2>
          <p>Genre: {book.genre}</p>
          <p>Author: {book.author.name}</p>
          <p>All books by author:</p>
          <ul className="OtherBooks">
            {book.author.books.map(b => <li key={b.id}>{b.name}</li>)}
          </ul>
        </div>
      )
    }
    else {
      return <div>No book selected</div>
    }
  }
  
  render() {
    return (
      this.props.data.loading ? <div>Loading...</div>
      : <div className="BookDetails">
          {this.displayBookDetails()}
        </div>
    );
  }
}

export default graphql(getBookQuery, {
  options: props => ({
    variables: {
      id: props.bookId,
    }
  })
})(BookDetails);