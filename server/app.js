const express = require('express');
const app = express();
const graphqlHTTP = require('express-graphql');
const mongoose = require('mongoose');
const cors = require('cors');

// allow X origin request
app.use(cors());

// DB (mLab.com mongoDB service)
const { DB_URL } = process.env;
if (DB_URL) {
  mongoose.connect(DB_URL, {useNewUrlParser: true});
  mongoose.connection.once('open', () => {
    console.log('connected to db');
  });
}
else {
  console.log('DB_URL env var not set');
}

// GraphQL
const schema = require('./schema/schema');

// Express
app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}));
app.listen(4000, () => console.log('Running on 4000'))